// Copyright (c) 2018-2024 Andrew Warkentin
//
// Based on code from Robigalia:
//
// Copyright (c) 2015 The Robigalia Project Developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.

//! Using endpoints for message passing
//!
//! In seL4, message passing is the fundamental primitive upon which the entire system is built.
//! All kernel services are accessed through messages sent to capabilities which the kernel
//! recognizes as belonging to kernel objects. Threads can also use this mechanism to send messages
//! between themselves.
//!
//! Endpoints represent authorization to receive or send messages for a particular queue. These
//! queues do not have a buffer, and act as a rendezvous. That is, senders block until there is a
//! receiver and receivers block until there is a sender - at which point they meet, the message is
//! copied directly from the source to its final destination, and the threads continue execution.
//! Multiple threads can be waiting to send or receive on the same queue. A message is delivered
//! from exactly one sender to exactly one receiver.
//!
//! In addition to being able to send data, capabilities can also be transfered between threads.
//! The endpoint must have the `CanGrant` bit set in its rights. In practice, only one new
//! capability can be transfered at a time - the actual situation is somewhat more complex. Refer
//! to §4.2.2 ("Capability Transfer") of the seL4 Reference Manual. The slot where the received
//! capability will be stored is global state not tied to any particular endpoint.
//!
//! Do note that `Endpoint` does not also attempt to model notification objects, instead leaving
//! that to the `Notification` type.

use core::slice;
use core::ops::{Deref, DerefMut};
use core::marker::PhantomData;
use sel4_sys::*;

use sel4_sys::seL4_LongMsgBuffer;

use ToCap;

use crate::Notification;

pub const LONG_IPC_OPTIONAL: usize = seL4_LongIPC_Optional as usize;
pub const LONG_IPC_GET_RECV_TOTAL_SIZE: usize = seL4_LongIPC_GetRecvTotalSize as usize;
pub const LONG_IPC_GET_REPLY_SIZE: usize = seL4_LongIPC_GetReplySize as usize;


cap_wrapper!{ ()
    /// An endpoint for message passing
    Endpoint = seL4_EndpointObject |_| 1 << seL4_EndpointBits,
    /// A reply object that holds a reply cap and scheduler context
    Reply = seL4_ReplyObject |_| 1 << seL4_ReplyBits,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SendLongMsgBuffer<'a> {
    pub base: *const c_types::c_void,
    pub len: seL4_Word,
    phantom: PhantomData<&'a [u8]>,
}

impl<'a> SendLongMsgBuffer<'a> {
    pub const fn new(data: &'a [u8]) -> SendLongMsgBuffer<'a> {
        SendLongMsgBuffer {
            base: data.as_ptr() as *const c_types::c_void,
            len: data.len(),
            phantom: PhantomData,
        }
    }
    pub const unsafe fn new_raw(base: *const u8, len: seL4_Word) -> SendLongMsgBuffer<'a> {
        SendLongMsgBuffer {
            base: base as *const c_types::c_void,
            len,
            phantom: PhantomData,
        }
    }
}

impl<'a> Deref for SendLongMsgBuffer<'a> {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe { slice::from_raw_parts(self.base as *const u8, self.len) }
    }
}

impl<'a> Default for SendLongMsgBuffer<'a> {
    fn default() -> Self {
        unsafe { Self::new_raw(0 as *const u8, 0) }
    }
}

unsafe impl<'a> Sync for SendLongMsgBuffer<'a> {}
unsafe impl<'a> Send for SendLongMsgBuffer<'a> {}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct RecvLongMsgBuffer<'a> {
    pub base: *mut c_types::c_void,
    pub len: seL4_Word,
    phantom: PhantomData<&'a mut [u8]>,
}

impl<'a> RecvLongMsgBuffer<'a> {
    pub const fn new(data: &'a mut [u8]) -> RecvLongMsgBuffer<'a> {
        RecvLongMsgBuffer {
            base: data.as_ptr() as *mut c_types::c_void,
            len: data.len(),
            phantom: PhantomData,
        }
    }
    pub const unsafe fn new_raw(base: *mut u8, len: seL4_Word) -> RecvLongMsgBuffer<'a> {
        RecvLongMsgBuffer {
            base: base as *mut c_types::c_void,
            len,
            phantom: PhantomData,
        }
    }
}

impl<'a> Deref for RecvLongMsgBuffer<'a> {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        unsafe { slice::from_raw_parts(self.base as *const u8, self.len) }
    }
}

impl<'a> DerefMut for RecvLongMsgBuffer<'a> {
    fn deref_mut(&mut self) -> &mut [u8] {
        unsafe { slice::from_raw_parts_mut(self.base as *mut u8, self.len) }
    }
}

impl<'a> Default for RecvLongMsgBuffer<'a> {
    fn default() -> Self {
        unsafe { Self::new_raw(0 as *mut u8, 0) }
    }
}

unsafe impl<'a> Sync for RecvLongMsgBuffer<'a> {}
unsafe impl<'a> Send for RecvLongMsgBuffer<'a> {}

/// The result of a successful receive.
///
/// Contains "sender information", which is the badge of the endpoint which was invoked to send a
/// message.
///
/// Also contains the decoded message information.
pub struct RecvToken {
    pub badge: seL4_Word,
    pub label: seL4_Word,
    caps_unwrapped: seL4_Word,
    len: seL4_Word,
    recv_len: seL4_Word,
    recv_total_len: seL4_Word,
    recv_reply_len: seL4_Word,
}

impl RecvToken {
    fn from_raw(sender: seL4_Word, message_info: seL4_MessageInfo, recv_len: seL4_Word, recv_total_len: seL4_Word, recv_reply_len: seL4_Word) -> RecvToken {
        RecvToken {
            badge: sender,
            label: unsafe { seL4_MessageInfo_get_label(message_info) },
            caps_unwrapped: unsafe { seL4_MessageInfo_get_capsUnwrapped(message_info) },
            len: unsafe { seL4_MessageInfo_get_length(message_info) },
            recv_len,
            recv_total_len,
            recv_reply_len,
        }
    }

    /// Read out unwrapped capabilities into a slice.
    ///
    /// Returns `Err` if the slice is not at least length `caps_unwrapped`.
    pub fn get_unwrapped_caps(&self, caps: &mut [seL4_Word]) -> Result<(), ()> {
        if caps.len() < seL4_MsgMaxExtraCaps as usize && caps.len() < self.caps_unwrapped{
            return Err(());
        }

        unsafe {
            ::core::intrinsics::copy_nonoverlapping(
                &(*seL4_GetIPCBuffer()).caps_or_badges as *const seL4_Word,
                caps.as_mut_ptr(),
                self.caps_unwrapped,
            )
        }

        Ok(())
    }

    /// Read out message data into a slice.
    ///
    /// Returns `Err` if the slice is not at least length `words_transferred`.
    pub fn get_data(&self, data: &mut [seL4_Word]) -> Result<(), ()> {
        if data.len() < seL4_MsgMaxLength as usize && data.len() < self.len {
            return Err(());
        }

        unsafe {
            ::core::intrinsics::copy_nonoverlapping(
                &(*seL4_GetIPCBuffer()).msg as *const seL4_Word, data.as_mut_ptr(),
                self.len as usize,
            )
        }

        Ok(())

    }

    pub fn caps_unwrapped(&self) -> usize {
        self.caps_unwrapped
    }

    pub fn words_transferred(&self) -> usize {
        self.len
    }
    pub fn recv_len(&self) -> usize {
        self.recv_len
    }
    pub fn recv_total_len(&self) -> usize {
        self.recv_total_len
    }
    pub fn recv_reply_len(&self) -> usize {
        self.recv_reply_len
    }
}

macro_rules! send_impls {
    ($name:ident) => {
impl $name {
    /// Send data.
    #[inline(always)]
    pub fn send_data(&self, label: seL4_Word, data: &[seL4_Word]) -> ::Result {
        self.send_message(label, data, &[])
    }

    /// Send a capability.
    #[inline(always)]
    pub fn send_cap<T: ::ToCap>(&self, label: seL4_Word, cap: T) -> ::Result {
        self.send_message(label, &[], &[cap.to_cap()])
    }

    /// Send a message.
    ///
    /// The only failures that can occur are if `data` or `caps` is too long to fit in the IPC
    /// buffer. In this case, `TooMuchData` or `TooManyCaps` will be the error details,
    /// respectively.
    ///
    /// This is `seL4_Send` in its full generality.
    #[inline(always)]
    pub fn send_message(&self, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr]) -> ::Result {
        if data.len() > seL4_MsgMaxLength as usize {
            return Err(::Error::from_details(::ErrorDetails::TooMuchData));
        }
        if caps.len() > seL4_MsgMaxExtraCaps as usize {
            return Err(::Error::from_details(::ErrorDetails::TooManyCaps));
        }
        unsafe {
            let buf = &mut*(seL4_GetIPCBuffer());
            ::core::ptr::copy_nonoverlapping(
                data.as_ptr(),
                buf.msg.as_mut_ptr(),
                data.len(),
            );
            ::core::ptr::copy_nonoverlapping(
                caps.as_ptr(),
                buf.caps_or_badges.as_mut_ptr(),
                caps.len()
            );
            seL4_Send(self.cptr, seL4_MessageInfo_new(label, 0, caps.len(), data.len()));
            Ok(())
        }
    }

    /// Raw send, using data already in the IPC buffer
    #[inline(always)]
    pub fn send(&self, label: seL4_Word, data: seL4_Word, caps: seL4_Word) {
        unsafe {
            seL4_Send(self.cptr, seL4_MessageInfo_new(label, 0, caps, data))
        };
    }

    /// Raw long send, using data already in the IPC buffer for the short part
    /// of the message
    #[inline(always)]
    pub fn long_send(&self, label: seL4_Word, short_len: seL4_Word, long_data: &[SendLongMsgBuffer], flags: seL4_Word) -> ::Result {
        let res = unsafe {
            seL4_LongSend(self.cptr, seL4_MessageInfo_new(label, 0, 0, short_len), long_data.as_ptr() as *const seL4_LongMsgBuffer, long_data.len(), flags)
        };
        let res = unsafe { seL4_MessageInfo_get_label(res) };
        if res != seL4_NoError as usize {
            Err(::Error(::GoOn::CheckIPCBuf { error_code: res as seL4_Error }))
        }else{
            Ok(())
        }

    }

    /// Raw non-blocking send, using data already in the IPC buffer
    #[inline(always)]
    pub fn try_send(&self, label: seL4_Word, data: seL4_Word, caps: seL4_Word) {
        unsafe {
            seL4_NBSend(self.cptr, seL4_MessageInfo_new(label, 0, caps, data))
        };
    }

    /// Try to send a message, returning no indication of failure if the message could not be sent.
    #[inline(always)]
    pub fn try_send_message(&self, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr]) -> ::Result {
        if data.len() > seL4_MsgMaxLength as usize {
            return Err(::Error::from_details(::ErrorDetails::TooMuchData));
        }
        if caps.len() > seL4_MsgMaxExtraCaps as usize {
            return Err(::Error::from_details(::ErrorDetails::TooManyCaps));
        }
        unsafe {
            let buf = &mut*(seL4_GetIPCBuffer());
            ::core::ptr::copy_nonoverlapping(
                data.as_ptr(),
                buf.msg.as_mut_ptr(),
                data.len(),
            );
            ::core::ptr::copy_nonoverlapping(
                caps.as_ptr(),
                buf.caps_or_badges.as_mut_ptr(),
                caps.len(),
            );
            seL4_NBSend(self.cptr, seL4_MessageInfo_new(label, 0, caps.len(), data.len()));
            Ok(())
        }
    }
}
}}

send_impls!(Endpoint);

impl Endpoint {
    /// Block until a message is received. Store reply cap and sched context in reply.
    #[inline(always)]
    pub fn recv(&self, reply: Reply) -> RecvToken {
        let mut sender = 0;
        let msginfo = unsafe { seL4_Recv(self.cptr, &mut sender, reply.to_cap()) };
        RecvToken::from_raw(sender, msginfo, 0, 0, 0)
    }

    /// Block until a message is received. Do not accept a reply cap or sched context.
    #[inline(always)]
    pub fn recv_refuse_reply(&self) -> RecvToken {
        let mut sender = 0;
        let msginfo = unsafe { seL4_Wait(self.cptr, &mut sender) };
        RecvToken::from_raw(sender, msginfo, 0, 0, 0)
    }

    /// Try to receive a message. Store reply cap and sched context in reply.
    ///
    /// If there is no message immediately available in the queue, the badge in `RecvToken` will be
    /// `0`. This is the only way to determine if a message was available.
    #[inline(always)]
    pub fn try_recv(&self, reply: Reply) -> RecvToken {
        let mut sender = 0;
        let msginfo = unsafe { seL4_NBRecv(self.cptr, &mut sender, reply.to_cap()) };
        RecvToken::from_raw(sender, msginfo, 0, 0, 0)
    }

    /// Try to receive a message. Do not accept a reply cap or sched context.
    ///
    /// If there is no message immediately available in the queue, the badge in `RecvToken` will be
    /// `0`. This is the only way to determine if a message was available.
    #[inline(always)]
    pub fn try_recv_refuse_reply(&self) -> RecvToken {
        let mut sender = 0;
        let msginfo = unsafe { seL4_NBWait(self.cptr, &mut sender) };
        RecvToken::from_raw(sender, msginfo, 0, 0, 0)
    }

    /// Block until a message is received, accepting a long buffer. Store
    /// reply cap and sched context in reply.
    #[inline(always)]
    pub fn long_recv(&self, reply: Reply, long_data: &[RecvLongMsgBuffer], flags: seL4_Word) -> Result<RecvToken, ::Error> {
        let mut sender = 0;
        let mut recv_size = 0;
        let mut recv_total_size = 0;
        let mut reply_size = 0;
        let msginfo = unsafe { seL4_LongRecv(self.cptr, &mut sender, reply.to_cap(), long_data.as_ptr() as *mut seL4_LongMsgBuffer, long_data.len(), &mut recv_size, &mut recv_total_size, &mut reply_size, flags) };
        check_transfer_error(msginfo)?;
        Ok(RecvToken::from_raw(sender, msginfo, recv_size, recv_total_size, reply_size))
    }

    /// Block until a message is received and return without transferring
    /// anything, leaving the message available for a later receive.
    #[inline(always)]
    pub fn poll(&self) -> Result<(), ::Error> {
        let mut sender = 0;
        let msginfo = unsafe { seL4_PollWait(self.cptr, &mut sender) };
        check_transfer_error(msginfo)?;
        Ok(())
    }

    /// Raw call, using data already in the IPC buffer
    ///
    /// Returns an error if the reply matches a defined kernel error code.
    ///
    /// CAUTION: There is no way to determine if the error code was sent by the kernel or the ipc
    /// partner.
    #[inline(always)]
    pub fn call(&self, label: seL4_Word, data: seL4_Word, caps: seL4_Word) -> Result<RecvToken, ::Error> {
        let msginfo = unsafe {
            seL4_Call(self.cptr, seL4_MessageInfo_new(label, 0, caps, data))
        };
        let recv_label = unsafe { seL4_MessageInfo_get_label(msginfo) & !(seL4_IsTransferError as usize) } as seL4_Error;
        if recv_label > 0 && recv_label < seL4_NumErrors {
            Err(::Error::from_ipcbuf(recv_label))
        } else {
            Ok(RecvToken::from_raw(0, msginfo, 0, 0, 0))
        }
    }
    /// Raw long call, using data already in the IPC buffer for the short
    /// portion of the message.
    ///
    /// Returns an error if the reply matches a defined kernel error code.
    ///
    /// CAUTION: There is no way to determine if the error code was sent by the kernel or the ipc
    /// partner.
    #[inline(always)]
    pub fn long_call(&self, label: seL4_Word, short_len: seL4_Word, send_long_data: &[SendLongMsgBuffer], recv_long_data: &[RecvLongMsgBuffer], flags: seL4_Word) -> Result<RecvToken, ::Error> {
        let mut long_msg_size = 0;
        let msginfo = unsafe {
            seL4_LongCall(self.cptr, seL4_MessageInfo_new(label, 0, 0, short_len), send_long_data.as_ptr() as *mut seL4_LongMsgBuffer, send_long_data.len(), recv_long_data.as_ptr() as *mut seL4_LongMsgBuffer, recv_long_data.len(), &mut long_msg_size, flags)
        };
        let recv_label = unsafe { seL4_MessageInfo_get_label(msginfo) & !(seL4_IsTransferError as usize) } as seL4_Error;
        if recv_label > 0 && recv_label < seL4_NumErrors {
            Err(::Error::from_ipcbuf(recv_label))
        } else {
            Ok(RecvToken::from_raw(0, msginfo, long_msg_size, long_msg_size, 0))
        }
    }
}

send_impls!(Reply);
impl Reply {
    pub fn read_long_buf(&self, bufs: &mut [RecvLongMsgBuffer], offset: usize) -> Result<seL4_Word, ::Error> {
        let mut transferred = 0;
        let msginfo = unsafe { seL4_LongReadBuf(self.to_cap(), bufs.as_mut_ptr() as *mut seL4_LongMsgBuffer, bufs.len(), offset, &mut transferred) };
        check_transfer_error(msginfo)?;
        Ok(transferred)
    }
    pub fn write_long_buf(&self, bufs: &[SendLongMsgBuffer], offset: usize) -> Result<seL4_Word, ::Error> {
        let mut transferred = 0;
        let msginfo = unsafe { seL4_LongWriteBuf(self.to_cap(), bufs.as_ptr() as *mut seL4_LongMsgBuffer, bufs.len(), offset, &mut transferred) };
        check_transfer_error(msginfo)?;
        Ok(transferred)
    }
}


#[inline]
fn check_transfer_error(msginfo: seL4_MessageInfo) -> Result<(), ::Error> {
    let recv_label = unsafe { seL4_MessageInfo_get_label(msginfo) };
    if recv_label as seL4_Word & seL4_IsTransferError as seL4_Word != 0 {
        let err = recv_label & !seL4_IsTransferError as usize;
        Err(::Error::from_ipcbuf(err as seL4_Error))
    }else{
        Ok(())
    }
}

/// Send a message thru a reply object, then block until a message is received
/// on this endpoint, storing a reply capability and sched context in the same
/// reply object.
///
/// The message must already be in the ipc buffer.
#[inline(always)]
pub fn reply_then_recv(reply: Reply, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint) -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let msginfo = unsafe { seL4_ReplyRecv(recv_on.to_cap(),
                                 seL4_MessageInfo_new(label, 0, caps, data),
                                 &mut sender,
                                 reply.to_cap()) };
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}

/// Send a long message thru a reply object, then block until a message is
/// received on this endpoint, storing a reply capability and sched context in
/// the same reply object.
///
/// The short portion of the message must already be in the ipc buffer.
#[inline(always)]
pub fn long_reply_then_recv(reply: Reply, label: seL4_Word, short_len: seL4_Word, recv_on: Endpoint, reply_long_data: &[SendLongMsgBuffer], recv_long_data: &[RecvLongMsgBuffer], flags: seL4_Word) -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let mut recv_size = 0;
    let mut recv_total_size = 0;
    let mut reply_size = 0;

    let msginfo = unsafe { seL4_LongReplyRecv(recv_on.to_cap(),
                                 seL4_MessageInfo_new(label, 0, 0, short_len),
                                 &mut sender,
                                 reply.to_cap(),
                                 reply_long_data.as_ptr() as *mut seL4_LongMsgBuffer,
                                 reply_long_data.len(),
                                 recv_long_data.as_ptr() as *mut seL4_LongMsgBuffer,
                                 recv_long_data.len(),
                                 &mut recv_size,
                                 &mut recv_total_size,
                                 &mut reply_size,
                                 flags) };
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, recv_size, recv_total_size, reply_size))
}

/// Try to Send a message thru a reply object. If another thread is not waiting
/// for the reply the message is dropped. Then block until a message is received
/// on an endpoint, storing a reply capability and sched context in the same
/// reply object.
///
/// The message must already be in the ipc buffer.
#[inline(always)]
pub fn try_reply_then_recv(reply: Reply, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint) -> Result<RecvToken, ::Error>  {
    raw_try_send_then_recv(reply.to_cap(), label, data, caps, recv_on, reply)
}

/// Try to Send a message thru an endpoint. If another thread is not waiting for
/// the message the message is dropped. Then block until a message is received
/// on an endpoint, storing a reply capability and sched context in reply.
///
/// The message must already be in the ipc buffer.
#[inline(always)]
pub fn try_send_then_recv(send_to: Endpoint, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint, reply: Reply) -> Result<RecvToken, ::Error> {
    raw_try_send_then_recv(send_to.to_cap(), label, data, caps, recv_on, reply)
}

#[inline(always)]
fn raw_try_send_then_recv(send_to: seL4_CPtr, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint, reply: Reply) -> Result<RecvToken, ::Error>  {
    let mut sender = 0;
    let msginfo = unsafe { seL4_NBSendRecv(send_to, seL4_MessageInfo_new(label, 0, caps, data),
                                  recv_on.to_cap(), &mut sender, reply.to_cap()) };
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}

/// Try to Signal a notification, then block until a message is received on an
/// endpoint, storing a reply capability and sched context in reply and copying
/// the long portion of the message into the provided long buffer.
///
/// The message must already be in the ipc buffer.
#[inline(always)]
pub fn try_signal_then_long_recv(to_signal: Notification, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint, reply: Reply, long_data: &[RecvLongMsgBuffer], flags: seL4_Word) -> Result<RecvToken, ::Error> {
    raw_try_send_then_long_recv(to_signal.to_cap(), label, data, caps, recv_on, reply, long_data, flags)
}

#[inline(always)]
fn raw_try_send_then_long_recv(send_to: seL4_CPtr, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint, reply: Reply, long_data: &[RecvLongMsgBuffer], flags: seL4_Word) -> Result<RecvToken, ::Error> {
    let mut sender = 0;

    let mut recv_size = 0;
    let mut recv_total_size = 0;
    let mut reply_size = 0;



    let msginfo = unsafe { seL4_LongNBSendRecv(send_to,
                        seL4_MessageInfo_new(label, 0, caps, data),
                        recv_on.to_cap(),
                        &mut sender,
                        reply.to_cap(),
                        long_data.as_ptr() as *mut seL4_LongMsgBuffer,
                        long_data.len(),
                        &mut recv_size,
                        &mut recv_total_size,
                        &mut reply_size,
                        flags) };
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, recv_size, recv_total_size, reply_size))
}

/// Try to Send a message thru an endpoint. If another thread is not waiting for
/// the message the message is dropped. Then block until a message is received
/// on an endpoint. Do not accept a reply cap or sched context.
///
/// The message must already be in the ipc buffer.
#[inline(always)]
pub fn try_send_then_recv_refuse_reply(send_to: Endpoint, label: seL4_Word, data: seL4_Word, caps: seL4_Word, recv_on: Endpoint) -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let msginfo = unsafe { seL4_NBSendWait(send_to.to_cap(),
                                  seL4_MessageInfo_new(label, 0, caps, data),
                                  recv_on.to_cap(),
                                  &mut sender) };
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}

/// Send a message thru a reply object, then block until a message is received
/// on this endpoint, storing a reply capability and sched context in the same
/// reply object.
#[inline(always)]
pub fn reply_message_then_recv(reply: Reply, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr], recv_on: Endpoint)
                       -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let msginfo;
    if data.len() > seL4_MsgMaxLength as usize {
        return Err(::Error::from_details(::ErrorDetails::TooMuchData));
    }
    if caps.len() > seL4_MsgMaxExtraCaps as usize {
        return Err(::Error::from_details(::ErrorDetails::TooManyCaps));
    }
    unsafe {
        let buf = &mut*(seL4_GetIPCBuffer());
        ::core::ptr::copy_nonoverlapping(
            data.as_ptr(),
            buf.msg.as_mut_ptr(),
            data.len(),
        );
        ::core::ptr::copy_nonoverlapping(
            caps.as_ptr(),
            buf.caps_or_badges.as_mut_ptr(),
            caps.len(),
        );

        msginfo = seL4_ReplyRecv(recv_on.to_cap(),
                                 seL4_MessageInfo_new(label, 0, caps.len(), data.len()),
                                 &mut sender,
                                 reply.to_cap());
    }
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}

/// Try to Send a message thru a reply object. If another thread is not waiting
/// for the reply the message is dropped. Then block until a message is received
/// on
/// an endpoint, storing a reply capability and sched context in the same reply
/// object.
#[inline(always)]
pub fn try_reply_message_then_recv(reply_to: Reply, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr],
                           recv_on: Endpoint)
                           -> Result<RecvToken, ::Error> {
    raw_try_send_message_then_recv(reply_to.to_cap(), label, data, caps, recv_on, reply_to)
}

/// Try to Send a message thru an endpoint. If another thread is not waiting for
/// the message the message is dropped. Then block until a message is received on
/// an endpoint, storing a reply capability and sched context in reply.
#[inline(always)]
pub fn try_send_message_then_recv(send_to: Endpoint, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr],
                          recv_on: Endpoint, reply: Reply)
                          -> Result<RecvToken, ::Error> {
    raw_try_send_message_then_recv(send_to.to_cap(), label, data, caps, recv_on, reply)
}

#[inline(always)]
fn raw_try_send_message_then_recv(send_to: seL4_CPtr, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr],
                          recv_on: Endpoint, reply: Reply)  -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let msginfo;
    if data.len() > seL4_MsgMaxLength as usize {
        return Err(::Error::from_details(::ErrorDetails::TooMuchData));
    }
    if caps.len() > seL4_MsgMaxExtraCaps as usize {
        return Err(::Error::from_details(::ErrorDetails::TooManyCaps));
    }
    unsafe {
        let buf = &mut*(seL4_GetIPCBuffer());
        ::core::ptr::copy_nonoverlapping(
            data.as_ptr(),
            buf.msg.as_mut_ptr(),
            data.len(),
        );
        ::core::ptr::copy_nonoverlapping(
            caps.as_ptr(),
            buf.caps_or_badges.as_mut_ptr(),
            caps.len(),
        );

        msginfo = seL4_NBSendRecv(send_to, seL4_MessageInfo_new(label, 0, caps.len(), data.len()),
                                  recv_on.to_cap(), &mut sender, reply.to_cap());
    }
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}

/// Try to Send a message thru an endpoint. If another thread is not waiting for
/// the message the message is dropped. Then block until a message is received on
/// an endpoint. Do not accept a reply cap or sched context.
#[inline(always)]
pub fn try_send_message_then_recv_refuse_reply(send_to: Endpoint, label: seL4_Word, data: &[seL4_Word], caps: &[seL4_CPtr],
                                       recv_on: Endpoint)  -> Result<RecvToken, ::Error> {
    let mut sender = 0;
    let msginfo;
    if data.len() > seL4_MsgMaxLength as usize {
        return Err(::Error::from_details(::ErrorDetails::TooMuchData));
    }
    if caps.len() > seL4_MsgMaxExtraCaps as usize {
        return Err(::Error::from_details(::ErrorDetails::TooManyCaps));
    }
    unsafe {
        let buf = &mut*(seL4_GetIPCBuffer());
        ::core::ptr::copy_nonoverlapping(
            data.as_ptr(),
            buf.msg.as_mut_ptr(),
            data.len(),
        );
        ::core::ptr::copy_nonoverlapping(
            caps.as_ptr(),
            buf.caps_or_badges.as_mut_ptr(),
            caps.len(),
        );

        msginfo = seL4_NBSendWait(send_to.to_cap(),
                                  seL4_MessageInfo_new(label, 0, caps.len(), data.len()),
                                  recv_on.to_cap(),
                                  &mut sender);
    }
    check_transfer_error(msginfo)?;
    Ok(RecvToken::from_raw(sender, msginfo, 0, 0, 0))
}
